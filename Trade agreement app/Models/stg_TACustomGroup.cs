//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trade_agreement_app.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class stg_TACustomGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public stg_TACustomGroup()
        {
            this.stg_TACustomGroupProducts = new HashSet<stg_TACustomGroupProducts>();
        }
    
        public int Id { get; set; }
        public string CustomGroupName { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDateTime { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<stg_TACustomGroupProducts> stg_TACustomGroupProducts { get; set; }
    }
}
