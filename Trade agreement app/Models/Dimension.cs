//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trade_agreement_app.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dimension
    {
        public int DimensionId { get; set; }
        public int DimensionTypeId { get; set; }
        public Nullable<int> DimensionValueId { get; set; }
        public int MychemID { get; set; }
    
        public virtual DimensionValue DimensionValue { get; set; }
        public virtual Product Product { get; set; }
        public virtual Product Product1 { get; set; }
    }
}
