﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trade_agreement_app.Models;
using System.Data.Entity;
using System.Net.NetworkInformation;
using System.IO;

using System.DirectoryServices.AccountManagement;

namespace Trade_agreement_app.Controllers
{
    public class HomeController : Controller
    {
        readonly StockDBEntities StockContext = new StockDBEntities();
        readonly CWMgtTradeAgreementEntities TradeContext = new CWMgtTradeAgreementEntities();
        readonly General_ReferenceEntities GeneralContext = new General_ReferenceEntities();
        readonly string TA_AdminType = System.Configuration.ConfigurationManager.AppSettings["TA_AdminType"];
        readonly string TA_UserType = System.Configuration.ConfigurationManager.AppSettings["TA_UserType"];
        public ActionResult Index()
        {
            UserListEnity CurrentUser = GetUserList(TA_AdminType, "admin");
            if (CurrentUser == null)
            {
                CurrentUser = GetUserList(TA_UserType, "user");
            }

            if (CurrentUser != null)
            {
                Session["Name"] = CurrentUser.Name;
                Session["GroupName"] = CurrentUser.GroupName;
                Session["UserRole"] = CurrentUser.UserRole;
                Session["EmailAddress"] = CurrentUser.EmailAddress;
            }
            else
            {
                Session["Name"] = null;
                Session["GroupName"] = null;
                Session["UserRole"] = null;
                Session["EmailAddress"] = null;
            }

            return View();
        }


        public UserListEnity GetUserList(string GrouName, string UserRole)
        {
            //var CurrentUser = UserPrincipal.Current.EmailAddress;
            List<UserListEnity> UserList = new List<UserListEnity>();
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

            UserPrincipal userTT = UserPrincipal.FindByIdentity(ctx, System.Environment.UserName);
            var CurrentUser = userTT.EmailAddress;
            using (var context = new PrincipalContext(ContextType.Domain, "mychemist"))
            {
                using (var group = GroupPrincipal.FindByIdentity(context, GrouName))
                {
                    if (group == null)
                    {
                        //MessageBox.Show("Group does not exist");
                    }
                    else
                    {
                        var users = group.GetMembers(true);
                        foreach (UserPrincipal user in users)
                        {
                            if (user.EmailAddress == CurrentUser)
                            {
                                UserListEnity UserEnity = new UserListEnity
                                {
                                    Name = user.Name,
                                    EmailAddress = user.EmailAddress,
                                    UserRole = UserRole,
                                    GroupName = group.Name
                                };
                                UserList.Add(UserEnity);

                            }
                            //user variable has the details about the user 

                        }
                    }
                }


            }
            return UserList.FirstOrDefault();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public JsonResult GetBranchList()
        {
            //List<int> branches = d.Split(',').Select(Int32.Parse).ToList();
            var res = GeneralContext.BranchInfoGlobals.Where(x=> x.CountryCode == "AUS" && x.StoreGroupID !=0).ToList();
            //var res1 =  (from s in StockContext.BranchInfoes where !branches.Contains(s.BranchID) select s).ToList();
            return Json(new { data = res }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPgList()
        {
            var res = StockContext.SubGroups.ToList(); //.Where(x=>(x.LockiePg != 126 && x.LockiePg != 127 && x.LockiePg != 128))
            List<SubGroupEntity> subGroupList = new List<SubGroupEntity>();
            foreach(var s in res)
            {
                SubGroupEntity sg = new SubGroupEntity
                {
                    Description = s.Description,
                    LockiePg = s.LockiePg,
                    ProductGroupId = s.ProductGroupId,
                    SubGroupId = s.SubGroupId
                };
                subGroupList.Add(sg);
            }
            return Json(new { data = subGroupList }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCustomPgList()
        {
            TradeContext.Configuration.ProxyCreationEnabled = false;
            var res = TradeContext.stg_TACustomGroup.OrderBy(x=>x.CustomGroupName).ToList();
            return Json(new { data = res }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetProductsList(string Description,int? MychemId)
        {
            StockContext.Configuration.ProxyCreationEnabled = false;
            if (MychemId != null)
            {
                var res = (from p in StockContext.Products
                           join sg in StockContext.SubGroups on p.SubGroup equals sg.LockiePg
                           where p.MychemID == MychemId && p.X_Product == false
                           select new ProductDetails { MychemID = p.MychemID, PLU = p.LockiePLU, Description = p.LockieDescription, PG = sg.Description, PGNumber = p.SubGroup }).Distinct();


                return Json(new { data = res }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //var res = (from p in StockContext.Products
                //           join sg in StockContext.SubGroups on p.SubGroup equals sg.LockiePg
                //           join d in StockContext.Dimensions on p.MychemID equals d.MychemID
                //           join dv in StockContext.DimensionValues on d.DimensionValueId equals dv.DimensionValueId
                //           where d.DimensionTypeId == 6 && dv.DimValue.Contains(Description) && p.X_Product == false
                //           select new ProductDetails { MychemID = p.MychemID, PLU = p.LockiePLU, Description = p.LockieDescription, PG = sg.Description, PGNumber = p.SubGroup }).Distinct();

                List<ProductDetails> res = StockContext.Database.SqlQuery<ProductDetails>("select p.MychemID, p.LockiePLU as PLU,p.Description,sg.Description as PG,p.SubGroup as PGNumber from Products p left join SubGroup sg on p.SubGroup = sg.LockiePg left join Dimensions D on p.MychemID = d.MychemID and d.DimensionTypeId = 6 join DimensionValues dv on d.DimensionValueId = dv.DimensionValueId where ((dv.DimValue LIKE '%' + REPLACE('" + Description + "', ' ', '%') + '%') or (p.Description LIKE '%' + REPLACE('" + Description + "', ' ', '%') + '%')) and X_Product =0 ").ToList();


                return Json(new { data = res }, JsonRequestBehavior.AllowGet);
            }
            
        }

        

        public JsonResult CreateCustomGroup(string customGroup)
        {
            var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName.ToUpper();
            var domain = domainName.Substring(0, domainName.IndexOf('.'));
            var userName = System.Environment.UserName;
            var CreatedBy = domain + "\\" + userName;
            var currentTime = DateTime.Now;
            stg_TACustomGroup CustomProductGroup = new stg_TACustomGroup
            {
                CustomGroupName = customGroup,
                CreatedDateTime = currentTime,
                CreatedBy = CreatedBy
            };
            var existingGroup = TradeContext.stg_TACustomGroup.Where(x => x.CustomGroupName.ToLower() == customGroup.ToLower()).FirstOrDefault();
            if(existingGroup == null)
            {
                TradeContext.Entry(CustomProductGroup).State = EntityState.Added;
                TradeContext.SaveChanges();
                var groupData = TradeContext.stg_TACustomGroup.Where(x => x.CustomGroupName.ToLower() == customGroup.ToLower()).FirstOrDefault();
                return Json(new { data = groupData }, JsonRequestBehavior.AllowGet);
            }
            
            return Json(new { data = "Group Name already exist." }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomGroupUpdateOnSourceTable(int customGroup,string GroupName,string Products)
        {
            var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName.ToUpper();
            var domain = domainName.Substring(0, domainName.IndexOf('.'));
            var userName = System.Environment.UserName;
            var CreatedBy = domain + "\\" + userName;

            var currentTime = DateTime.Now;
            List<int> MychemIDs = Products.Split(',').Select(Int32.Parse).ToList();
            foreach(var chemId in MychemIDs)
            {
                stg_TACustomGroupProducts ct = new stg_TACustomGroupProducts
                {
                    CustomGroupId = customGroup,
                    MychemID = chemId,
                    //LastModifiedDateTime = currentTime,
                    CreatedBy = CreatedBy,
                    CreatedDateTime = currentTime
                };
                var existingItem = TradeContext.stg_TACustomGroupProducts.Where(x => x.MychemID == chemId).FirstOrDefault();
                if(existingItem != null)
                {
                    existingItem.CustomGroupId = customGroup;
                    existingItem.LastModifiedDateTime = currentTime;
                    existingItem.LastModifiedBy = CreatedBy;
                    TradeContext.Entry(existingItem).State = EntityState.Modified;
                }
                else
                {
                    TradeContext.Entry(ct).State = EntityState.Added;
                }
                
            }
            TradeContext.SaveChanges();
            var RemoveMychemIds = TradeContext.stg_TACustomGroupProducts.Where(x=>x.CustomGroupId == customGroup).ToList();
            foreach (var item in RemoveMychemIds)
            {
                var res = (MychemIDs.IndexOf(item.MychemID) != -1);
                if (MychemIDs.IndexOf(item.MychemID) == -1 && customGroup == item.CustomGroupId)
                {
                    TradeContext.stg_TACustomGroupProducts.Remove(item);
                }
                    
            }
            TradeContext.SaveChanges();
            return Json(new { data = "Custom Group successfully created." }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateTradeOnBranchIfoProductsTable(List<stg_TradeAgreementsRules> TgArr)
        {
            
            var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName.ToUpper();
            var domain = domainName.Substring(0, domainName.IndexOf('.'));
            var userName = System.Environment.UserName;
            

            string CreatedBy = "";
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name;
            }
            else
            {
                //CreatedBy = domain + "\\" + Environment.UserName; //domain + "\\" + userName;
                CreatedBy = Path.Combine(domain, Environment.UserName);
            }
            var currentTime = DateTime.Now;
            foreach (var tdr in TgArr)
            {
                var r = TradeContext.stg_TradeAgreementsRules.Where(x => (x.StoreType == tdr.StoreType && x.StoreTypeValue == tdr.StoreTypeValue && x.ProductType == tdr.ProductType && x.ProductTypeValue == tdr.ProductTypeValue)).FirstOrDefault();
                if(r == null)
                {
                    tdr.CreatedBy = CreatedBy;
                    tdr.CreatedDateTime = currentTime;
                    TradeContext.Entry(tdr).State = EntityState.Added;
                }
                else
                {
                    r.StoreType = tdr.StoreType;
                    r.StoreTypeValue = tdr.StoreTypeValue;
                    r.ProductType = tdr.ProductType;
                    r.ProductTypeValue = tdr.ProductTypeValue;
                    r.isApplyTA = tdr.isApplyTA;
                    r.DiscountPCT = tdr.DiscountPCT;
                    r.Priority = tdr.Priority;
                    r.LastModifiedDateTime = currentTime;
                    r.CreatedBy = r.CreatedBy == null ? CreatedBy : r.CreatedBy;
                    r.LastModifiedBy = CreatedBy;
                    TradeContext.Entry(r).State = EntityState.Modified;
                }
            }
            TradeContext.SaveChanges();
            return Json(new { data = "Trade agreement updated successfully."+ CreatedBy }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPriceDetailsOnDesc(string Store, string desc)
        {
            List<object> priceList = new List<object>();
            //List<Product> ProductList = StockContext.Products.Where(x => x.Description.ToLower().Contains(desc.ToLower()) && x.X_Product == false).ToList();
            List<Product> ProductList = StockContext.Database.SqlQuery<Product>("select * from Products where X_Product=0 and Description LIKE '%' + REPLACE('" + desc + "', ' ', '%') + '%'").ToList();
            if (ProductList.Count>0)
            {
                foreach(var i in ProductList)
                {
                    priceList.Add(GetPriceDetails(Store, i.MychemID).Data);
                }
                

            }
            return Json(new { data = priceList }, JsonRequestBehavior.AllowGet); ;
        }


        public JsonResult GetPriceDetails(string Store, int MychemId)
        {
            var Product = StockContext.Products.Where(x => x.MychemID == MychemId).FirstOrDefault();
            var Product_ex = StockContext.vwProducts_NDSS_PrePack.Where(x => x.MychemID == MychemId).FirstOrDefault();
            int StrId = Int16.Parse(Store);
            var StoreObj = GeneralContext.BranchInfoGlobals.Where(x => x.BranchID == StrId).FirstOrDefault();
            double? TradeValue = 0;
            if (Product == null)
            {
                return Json(new { data = "Invalid MychemID" }, JsonRequestBehavior.AllowGet);
            }

            List<stg_TradeAgreementsRules> ExclusionCheck = TradeContext.stg_TradeAgreementsRules.Where(x => x.Priority == 0).ToList();
            stg_TACustomGroupProducts CustomDetails = TradeContext.stg_TACustomGroupProducts.Where(x => x.MychemID == MychemId).FirstOrDefault();
            //if (ExclusionCheck.Where(x=>x.ProductTypeValue == Product.SubGroup).ToList().Count>0 || ExclusionCheck.Where(x => (x.StoreType == "SC" && StoreObj.CountryCode == x.StoreTypeValue.ToString())).ToList().Count > 0)
            if (ExclusionCheck.Where(x=>(x.ProductType.ToLower() == "schedule" && x.ProductTypeValue == Product_ex.Schedule)).ToList().Count>0 ||
             ExclusionCheck.Where(x=>(x.ProductType.ToLower() == "ndss" && Product_ex.NDSS == 1)).ToList().Count>0 ||
             ExclusionCheck.Where(x=>(x.ProductType.ToLower() == "serviceitemexclprepack" && Product_ex.ServiceItem == true && Product_ex.PrePack == 0)).ToList().Count>0 ||
             ExclusionCheck.Where(x=>(x.ProductType == "CG" && (CustomDetails != null && x.ProductTypeValue == CustomDetails.CustomGroupId))).ToList().Count>0 ||
                ExclusionCheck.Where(x => (x.StoreType == "SC" && StoreObj.CountryCode == x.StoreTypeValue.ToString())).ToList().Count > 0 ||
                ExclusionCheck.Where(x => (x.ProductType.ToLower() == "pg"  && x.ProductTypeValue == Product.SubGroup)).ToList().Count > 0 
                )
            {
                TradeValue = -1;
            }
            else
            {
                List<stg_TradeAgreementsRules> res = TradeContext.stg_TradeAgreementsRules.Where(x => x.StoreType == "ST" && x.StoreTypeValue == StrId).ToList();
                
                int GroupId = 0;
                if (CustomDetails != null)
                {
                    GroupId = TradeContext.stg_TACustomGroup.Where(x=>x.Id == CustomDetails.CustomGroupId).FirstOrDefault().Id;
                }

                var c = res.Find(x => x.ProductType == "CG" && x.ProductTypeValue == GroupId);
                if (c != null)
                {
                    TradeValue = c.DiscountPCT;
                }
                else
                {
                    var d = res.Find(x => x.ProductType == "PG" && x.ProductTypeValue == Product.SubGroup);
                    if (d != null)
                    {
                        TradeValue = d.DiscountPCT;
                    }
                    else
                    {
                        var e = res.Find(x => x.ProductType == "ALL");
                        if(e != null)
                        {
                            TradeValue = e.DiscountPCT;
                        }
                        else
                        {
                            List<stg_TradeAgreementsRules> res1 = TradeContext.stg_TradeAgreementsRules.Where(x => x.StoreType == "SG" && x.StoreTypeValue == StoreObj.StoreGroupID).ToList();
                            var f = res1.Find(x => x.ProductType == "CG" && x.ProductTypeValue == GroupId);
                            if (f != null)
                            {
                                TradeValue = f.DiscountPCT;
                            }
                            else
                            {
                                var g = res1.Find(x => x.ProductType == "PG" && x.ProductTypeValue == Product.SubGroup);
                                if (g != null)
                                {
                                    TradeValue = g.DiscountPCT;
                                }
                                else
                                {
                                    var h = res1.Find(x => x.ProductType == "ALL");
                                    if(h != null)
                                    {
                                        TradeValue = h.DiscountPCT;
                                    }
                                    else
                                    {
                                        List<stg_TradeAgreementsRules> res2 = TradeContext.stg_TradeAgreementsRules.Where(x => x.StoreType == "all").ToList();
                                        var i = res2.Find(x => x.ProductType == "CG" && x.ProductTypeValue == GroupId);
                                        if (i != null)
                                        {
                                            TradeValue = i.DiscountPCT;
                                        }
                                        else
                                        {
                                            var pg = Product.SubGroup.ToString();
                                            var j = res2.Find(x => x.ProductType == "PG" && x.ProductTypeValue == Product.SubGroup);
                                            if (j != null)
                                            {
                                                TradeValue = j.DiscountPCT;
                                            }
                                            else
                                            {
                                                var k = res2.Find(x => x.ProductType == "ALL");
                                                TradeValue = k!=null?k.DiscountPCT:-1;
                                            }
                                        }
                                        
                                    }
                                            
                                }
                            }
                        }
                            
                    }
                }

            }


            //var basePriceObj = StockContext.ProductPricingMatrices.Where(x => x.MychemID == MychemId && x.PriceLevel == 1 && x.StoreGroupID == StoreObj.StoreGroupID).FirstOrDefault();

            //changed on 
            //var basePriceObj = StockContext.Database.SqlQuery<ProductPricingMatrix>("select top 1 PM.* from [dbo].[ProductPricingMatrix] PM inner join [dbo].[BranchCatGroups] BC on PM.StoreGroupID = BC.CatGroup inner join StoreGroup sg on pm.StoreGroupID = sg.StoreGroupId WHERE PM.PriceLevel = 1 AND MychemID= " + "'" + MychemId + "'" + " AND BC.BranchID = " + "'" + StrId + "'" + " order by  PM.StoreGroupID desc").FirstOrDefault();

            //var basePriceObj = StockContext.Database.SqlQuery<ProductPricingMatrix>("select top 1 PM.* from [dbo].[ProductPricingMatrix] PM inner join [dbo].[BranchInfoGlobal] BC on PM.StoreGroupID = BC.StoreGroupID WHERE PM.PriceLevel = 1 AND MychemID= " + "'" + MychemId + "'" + " AND BC.BranchID = " + "'" + StrId + "'" + " order by  PM.StoreGroupID desc").FirstOrDefault();
            var basePriceObj = StockContext.Database.SqlQuery<ProductPricingDTO>("select top 1 PM.MychemID,PM.BasePriceExGST,PM.StoregroupID from [dbo].[ProductPricingEDLPExGSTCache] PM inner join [dbo].[BranchInfoGlobal] BC on PM.StoreGroupID = BC.StoreGroupID WHERE MychemID= " + "'" + MychemId + "'" + " AND BC.BranchID = " + "'" + StrId + "'" + " order by  PM.StoreGroupID desc").FirstOrDefault();

            float? price = null;
            if(basePriceObj != null)
            {
                //if(Product.GstOut == false)
                //{
                //    price = basePriceObj.Price;
                //}
                //else
                //{
                //    price = basePriceObj.Price / (float)1.1;
                //}

                price = basePriceObj.BasePriceExGST;
            }
            else
            {
                return Json(new { data = "No Data Found." }, JsonRequestBehavior.AllowGet);
            }
            
            //var catItem = StockContext.Database.SqlQuery<ActiveCatalogItem>("select top 1 * from ActiveCatalogItems  WHERE StartDate <= getdate() AND EndDate >= getdate() AND MychemID=" + "'" + MychemId + "'" + " AND CatGroup=" + "'" + StoreObj.StoreGroupID + "'" + "order by LastUpdate desc").FirstOrDefault();
            var catItem = StockContext.Database.SqlQuery<ActiveCatalogItem>("select top 1 AC.* from [dbo].[ActiveCatalogItems] AC inner join [dbo].[BranchCatGroups] BC on AC.CatGroup = BC.CatGroup WHERE AC.StartDate <= getdate() AND AC.EndDate >= getdate() AND MychemID= " + "'" + MychemId + "'" + " AND BC.BranchID = " + "'" + StrId + "'" + " order by  AC.CatGroup desc,  AC.LastUpdate desc").FirstOrDefault();
            double? catPrice = price;
            double? Rebate = 0;
            double? Cal_Cost_price = 0;
            if (TradeValue == -1 || TradeValue == null)
            {
                TradeValue = -1;
                Cal_Cost_price = null;
                Rebate = null;
                if (catItem != null)
                {
                    if (Product.GstOut == false)
                    {
                        catPrice = (double)catItem.CatPrice;
                    }
                    else
                    {
                        catPrice = (double)(catItem.CatPrice / (decimal)1.1);
                    }
                }
            }
            else
            {
                if (catItem != null)
                {
                    if (Product.GstOut == false)
                    {
                        catPrice = (double)catItem.CatPrice;
                    }
                    else
                    {
                        catPrice = (double)(catItem.CatPrice / (decimal)1.1);
                    }
                    var rbt = (catPrice * TradeValue / 100) - (catPrice - (price - (price * TradeValue / 100)));
                    Rebate = (double)(Math.Round((double)rbt, 2));
                }
                else
                {
                    Rebate = 0;
                }
                Cal_Cost_price = (double)(Math.Round((double)(price - (price * TradeValue / 100)), 2));
                //var OrigPrice = Math.Round((double)price, 2);
                //Cal_Cost_price = (double)(Math.Round((double)(OrigPrice - (OrigPrice * TradeValue / 100)), 2));
            }
            
            PricingEntity priceTrade = new PricingEntity();

            priceTrade.Cost_price = Cal_Cost_price;
            priceTrade.Base_price = (double)(Math.Round((double)price, 2));
            priceTrade.Sell_price = (double)(Math.Round((double)catPrice, 2));  
            priceTrade.Rebate = Rebate;
            priceTrade.Trade = TradeValue;
            priceTrade.MychemId = Product.MychemID;
            priceTrade.Description = Product.Description;
            if(Product.GstIn == false && Product.GstOut == false)
            {
                priceTrade.gst = "free";
            } else if(Product.GstIn == true && Product.GstOut == false)
            {
                priceTrade.gst = "No";
            }
            else if (Product.GstOut == true)
            {
                priceTrade.gst = "Yes";
            }
            return Json(new { data = priceTrade }, JsonRequestBehavior.AllowGet);
        }

        public string GetBrandName(int? digit)
        {
            switch (digit)
            {
                case 1: return "My Chemist";
                case 2: return "Chemist Warehouse";
                case 3: return "My Beauty Spot";
            }
            return "";
        }

        public int GetBrandId(string digit)
        {
            switch (digit)
            {
                case "My Chemist": return 1;
                case "Chemist Warehouse": return 2;
                case "My Beauty Spot": return 3;
            }
            return -1;
        }


        public JsonResult GetPriotityList()
        {
            List<stg_TradeAgreementsRulesDTO> TradeList = TradeContext.Database.SqlQuery<stg_TradeAgreementsRulesDTO> ("select stg.*,postRule =(case when p.Id is null then 'New' else 'Exist' end) ,synced = (case when (p.Id is not null and isnull(stg.DiscountPCT,0) = isnull(p.DiscountPCT,0) and stg.isApplyTA = p.isApplyTA) then 'true' else 'false' end) from stg_TradeAgreementsRules stg left join Post_TradeAgreementsRules p on stg.Id = p.Id").ToList(); // TradeContext.stg_TradeAgreementsRules.ToList();
            return Json(new { data = TradeList.OrderBy(x => x.Priority) }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCustomProductsList(int customGroupId)
        {
            List<stg_TACustomGroupProducts> CgList = TradeContext.stg_TACustomGroupProducts.Where(x=>x.CustomGroupId == customGroupId).Take(100).ToList();
            int gId = CgList[0].CustomGroupId;
            var CgName = TradeContext.stg_TACustomGroup.Where(x => x.Id == gId).FirstOrDefault().CustomGroupName;
            List<CgListEntity> CustomList = new List<CgListEntity>(); 
            foreach(var cg in CgList)
            {
                CgListEntity CustomGroup = new CgListEntity
                {
                    GroupId = cg.CustomGroupId,
                    MychemID = cg.MychemID,
                    GroupName = CgName
                };
                CustomList.Add(CustomGroup);
            }
            return Json(new { data = CustomList }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CheckCustomGroupIds(string MychemIds,int? GroupId)
        {
            TradeContext.Configuration.ProxyCreationEnabled = false;
            List<int> MychemIDList = MychemIds.Split(',').Select(Int32.Parse).ToList();
            List<stg_TACustomGroupProducts> CgList = new List<stg_TACustomGroupProducts>();
            if(GroupId == null)
            {
                CgList = TradeContext.stg_TACustomGroupProducts.Where(x => MychemIDList.Contains(x.MychemID)).ToList();
            }
            else
            {
                CgList = TradeContext.stg_TACustomGroupProducts.Where(x => MychemIDList.Contains(x.MychemID) && x.CustomGroupId != GroupId).ToList();
            }
                
            return Json(new { data = CgList }, JsonRequestBehavior.AllowGet);
        }
        
        
         public JsonResult DeleteCustomProductsList(int customGroupId)
         {
            var cg = TradeContext.stg_TACustomGroup.Where(x => x.Id == customGroupId).FirstOrDefault();
            var RemoveMychemIds = TradeContext.stg_TACustomGroupProducts.Where(x => x.CustomGroupId == customGroupId).ToList();
            foreach (var item in RemoveMychemIds)
            {
                TradeContext.stg_TACustomGroupProducts.Remove(item);

            }
            TradeContext.SaveChanges();
            if (cg != null)
            {
                TradeContext.stg_TACustomGroup.Remove(cg);
                TradeContext.SaveChanges();
            }

            var RemovePriorityRules = TradeContext.stg_TradeAgreementsRules.Where(x => x.ProductType == "CG" && x.ProductTypeValue == customGroupId).ToList();
            foreach (var item in RemovePriorityRules)
            {
                TradeContext.stg_TradeAgreementsRules.Remove(item);

            }
            TradeContext.SaveChanges();
            return Json(new { data = "Custom Group successfully deleted." }, JsonRequestBehavior.AllowGet);
         }

        public JsonResult DeleteTradeRule(int Id)
        {
            

            var RemovePriorityRule = TradeContext.stg_TradeAgreementsRules.Where(x => x.Id == Id).FirstOrDefault();
            TradeContext.stg_TradeAgreementsRules.Remove(RemovePriorityRule);
            TradeContext.SaveChanges();
            return Json(new { data = "Trade Agreement rule successfully deleted." }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SyncChanges()
        {
            //var sync = TradeContext.usp_PostTradeAgreement();
            var sync = TradeContext.Database.SqlQuery<int>("DECLARE @return_value int; EXEC @return_value =[dbo].[usp_PostTradeAgreement]; SELECT	'Return Value' = @return_value").FirstOrDefault();
            return Json(new { data = sync }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetDeletedPriotityList()
        {
            List<stg_TradeAgreementsRulesDTO> TradeList = TradeContext.Database.SqlQuery<stg_TradeAgreementsRulesDTO>("select stg.*,postRule =(case when p.Id is null then 'Delete' else 'Exist' end) ,synced = (case when (p.Id is not null and isnull(stg.DiscountPCT,0) = isnull(p.DiscountPCT,0) and stg.isApplyTA = p.isApplyTA) then 'true' else 'false' end) from Post_TradeAgreementsRules  stg left join stg_TradeAgreementsRules p on stg.Id = p.Id").Where(x=>x.postRule=="Delete").ToList(); // TradeContext.stg_TradeAgreementsRules.ToList();
            return Json(new { data = TradeList.OrderBy(x => x.Priority) }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetPostCustomPgList()
        {
            TradeContext.Configuration.ProxyCreationEnabled = false;
            var res = TradeContext.Database.SqlQuery<stg_TACustomGroup>("select * from Post_TACustomGroup").OrderBy(x => x.Id).ToList();
            return Json(new { data = res }, JsonRequestBehavior.AllowGet);
        }

        public class PricingEntity
        {
            public int MychemId { get; set; }
            public string Description { get; set; }
            public double? Trade { get; set; }
            public double? Sell_price { get; set; }
            public double? Base_price { get; set; }
            public double? Cost_price { get; set; }
            public double? Rebate { get; set; }
            public string gst { get; set; }
        }

        public class CgListEntity
        {
            public string GroupName { get; set; }
            public int MychemID { get; set; }
            public int GroupId  { get; set; }
        }

        public class ProductDetails
        {
            public int MychemID { get; set; }
            public string Description { get; set; }
            public int? PLU { get; set; }
            public string PG { get; set; }
            public int? PGNumber { get; set; }

        };


        public class SubGroupEntity
        {
            public int SubGroupId { get; set; }
            public Nullable<int> ProductGroupId { get; set; }
            public string Description { get; set; }
            public Nullable<int> LockiePg { get; set; }

        };

        public class UserListEnity
        {
            public string Name { get; set; }
            public string EmailAddress { get; set; }
            public string GroupName { get; set; }
            public string UserRole { get; set; }

        }



        public partial class stg_TradeAgreementsRulesDTO
        {
            public int Id { get; set; }
            public string StoreType { get; set; }
            public Nullable<int> StoreTypeValue { get; set; }
            public string ProductType { get; set; }
            public Nullable<int> ProductTypeValue { get; set; }
            public Nullable<System.DateTime> from_date { get; set; }
            public Nullable<System.DateTime> to_date { get; set; }
            public Nullable<bool> isApplyTA { get; set; }
            public Nullable<double> DiscountPCT { get; set; }
            public Nullable<int> Priority { get; set; }
            public string CreatedBy { get; set; }
            public System.DateTime CreatedDateTime { get; set; }
            public string LastModifiedBy { get; set; }
            public Nullable<System.DateTime> LastModifiedDateTime { get; set; }

            public string synced { get; set; }
            public string postRule { get; set; }

        }


        public class ProductPricingDTO
        {
            public int MychemID { get; set; }
            public float? BasePriceExGST { get; set; }

            public int StoreGroupID { get; set; }

        }
    }
}