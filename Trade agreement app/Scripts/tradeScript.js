﻿if (!Array.prototype.find) {
    Array.prototype.find = function (predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

$(document).ready(function () {
    var UserRole = $('#UserRole').val().toLowerCase();
    var BranchList = [], CustomGroupList = [], PgList = [], PriotiryRulesList = [];
    LoadBranchList();
    LoadPgList();
    getCustomPgList();
    
  

    $("form").submit(function (e) {
        e.preventDefault();
        return false;
    });

    $('#clear_td').click(function () {
        $('#clear_td').text('Clear');
        $(".ams_stores select option,.ams_pg select option").prop("selected", false);
        $(".store_name_sec input[type='radio'],.group_name_sec input[type='radio']").prop("checked", false);
        $("#few_pg select,#few_stores select,#all_stores select").multiselect("refresh");
        $('.show_store_type > *, .show_group_name >*').slideUp();
        $('#td').val('').prop('disabled',false);
        $("#reset-td").prop("checked", false).prop('disabled', false);
        $('.priority-table tr').removeClass('active-row');
        $('.ta-form').removeClass('activeEditForm');
        $('.ta-form').nextAll().css('pointer-events', 'all');
        $('.create-new-pg').show();
        $('.edit-custom-pg,.delete-custom-pg').hide();
    });

    //Load Stores List
    function LoadBranchList() {
        $.ajax({
            cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetBranchList",
            dataType: "json",
            success: function (res) {
                //console.log(res);
                if (res.data && res.data.length) {
                    BranchList = res.data;
                    var branchStr = '';
                    $.each(res.data, function (k, v) {
                        branchStr += '<option value=' + v.BranchID + '>' + (v.BranchID+' - ' + v.Storename + ' - '+v.AxAccountCode) + '</option>';
                    });
                    $('.ams_stores select').html(branchStr);
                    $('.ams_stores select').multiselect({
                        numberDisplayed: 1,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 250
                    });
                }
            },
            error: function (result, xhr) {
            }
        });
    }

    //Load Product Groups list
    function LoadPgList() {
        $.ajax({
            cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetPgList",
            dataType: "json",
            success: function (res) {
                $('.loading-bar-section').hide();
                //console.log(res);
                if (res.data && res.data.length) {
                    PgList = res.data;
                    var branchStr = '';
                    $.each(res.data, function (k, v) {
                        branchStr += '<option value=' + v.LockiePg + '>' + (v.LockiePg+' - '+v.Description) + '</option>';
                    });
                    $('#few_pg select').html(branchStr);
                    $('#few_pg select').multiselect({
                        numberDisplayed: 1,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 250
                    });
                    console.log('in');
                    // $('#few_stores select').multiselect("refresh");
                }

            },
            error: function (result, xhr) {
                $('.loading-bar-section').hide();
            }
        });
    }



    //Load Custom Product group list

    function getCustomPgList() {
        $.ajax({
            //cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetCustomPgList",
            dataType: "json",
            success: function (res) {
                //console.log(res);
                var branchStr = '<option value="">-select-</option>';
                $.each(res.data, function (k, v) {
                    branchStr += '<option value=' + v.Id + '>' + v.CustomGroupName + '</option>';
                });
                $('.select-custom-pg').html(branchStr);
                CustomGroupList = res.data;
            },
            error: function (result, xhr) {
            }
        });
    }

    //Store radio button selection
    $('.store_name_sec input[type="radio"], .group_name_sec input[type="radio"]').change(function () {
        $('.show_' + $(this).attr('name') + '>*').hide();
        if ($(this).attr('selected_type')) {
            $('#' + $(this).attr('selected_type')).slideDown();
        }

    });

    // Custom Product selection
    $('.select-custom-pg').on('change', function () {
        if ($(this).find('option:selected').val() == 0) {
            $('.create-new-pg').show();
            $('.edit-custom-pg,.delete-custom-pg').hide();
            
        } else {
            $('.create-new-pg').hide();
            $('.edit-custom-pg,.delete-custom-pg').show();

        }
    });


    //Opens new Custom Product group Modal
    $('.create-new-pg').on('click', function () {
        $('#new-custom-pg').prop('disabled',false);
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('.new-custom-group').show();
        $('.save-pg').addClass('create-new-cg');
        duplicatePassThrough = true;
        $('.save-pg').text('Create');
        $('.modal-title>span').text('Create ');
    });


    //Opens new Custom Product group Modal
    $('.edit-custom-pg').on('click', function () {

        $.ajax({
            //cache: false,
            //async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetCustomProductsList?customGroupId=" + $('.select-custom-pg').find('option:selected').val(),
            dataType: "json",
            success: function (res) {

                var CgList = res.data;
                console.log(CgList);
                $('#new-custom-pg').val(CgList[0].GroupName).prop('disabled', true);
                var str = '';
                for (var i = 0; i < CgList.length; i++) {

                    str += '<p><span>' + CgList[i].MychemID + '</span><i class="glyphicon glyphicon-remove remove-product"></i></p>';
                }
                $('#new-custom-pg-items').append(str);
                $('#myModal').modal({
                    backdrop: 'static',
                    keyboard: false
                })
                $('.new-custom-group').show();
                $('.save-pg').addClass('edit-cg');
                duplicatePassThrough = true;
                $('.save-pg').text('Update');
                $('.modal-title>span').text('Edit ');
            },
            error: function () {

            }
        });

        
    });


    //Delete Custom PG
    $('.delete-custom-pg').on('click', function () {
        var CG = $('.select-custom-pg').find('option:selected').val();

        var ruleCheck = PriotiryRulesList.filter(x => x.ProductType == 'CG' && x.ProductTypeValue == CG);
        if (ruleCheck.length) {
            if (confirm('There are Trade Agreement Rules set using this Custom Group. Deleting this Custom Group will delete any associated rules')) {
                // Save it!
                deleteCustomGroup(CG);
            } else {
                // Do nothing!
            }
        } else {
            deleteCustomGroup(CG);
        }
        
    });


    function deleteCustomGroup(CG) {
        $('.loading-bar-section').show();
        $.ajax({
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/DeleteCustomProductsList?customGroupId=" + CG,
            dataType: "json",
            success: function (res) {
                $('.loading-bar-section').hide();
                getCustomPgList();
                showNotification("Custom group successfully deleted!", 'success', 'center');
                $('.create-new-pg').show();
                $('.edit-custom-pg,.delete-custom-pg').hide();
                loadPriorityTable(true);
            },
            error: function () {

            }
        });
    }

    /**
     * Custom PG creation 
     */

    //Search Products by description

    $(".search-pg").click(function () {
        if ($("#custom-pg").val().length >= 3) {
            searchProductsByGroupName($("#custom-pg").val());
        }
    });

    //Search query on enter key
    $("#custom-pg").on('keyup', function (e) {
        var keyCode = e.keyCode || e.charCode;
        if (keyCode == 13 && $(this).val().length >= 3) {
            //$("#myModal").modal();
            searchProductsByGroupName($(this).val())
        }
    });
    //Selection of Products into Custom Group
    $('.add-new-products').click(function () {
        var selRows = jQuery("#myGrid").jqGrid('getGridParam', 'selarrrow');
        console.log(selRows);
        if (selRows.length) {
            var existingProducts = $('#new-custom-pg-items>p').map(function () {
                return $(this).find('span').text()
            }).toArray();
            for (var i = 0; i < selRows.length; i++) {
                var rowData = $("#myGrid").jqGrid("getRowData", selRows[i]);
                console.log(rowData);
                if (existingProducts.indexOf(rowData.MychemID) == -1) {
                    $('#new-custom-pg-items').append('<p><span>' + rowData.MychemID + '</span><i class="glyphicon glyphicon-remove remove-product"></i></p>');
                }
            }
        } else {
            alert('please select the products')
        }
    });

    //Remove Product from Custom Product group
    $('#new-custom-pg-items').on('click', '.remove-product', function () {
        $(this).parent().remove();
    });

    $('[type="date"]#from_date,[type="date"]#to_date').prop('min', function () {
        return new Date().toJSON().split('T')[0];
    });
    //Search Products by description in custom pg modal
    function searchProductsByGroupName(pgName) {
        $('.modal-icon-sec').show();
        var description = MychemId = null;

        if (isNaN(pgName)) {
            description = pgName;
        } else {
            MychemId = pgName;
        }

        $.ajax({
            //cache: false,
            //async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetProductsList?Description=" + description + "&&MychemId=" + MychemId,
            dataType: "json",
            success: function (res) {
                $('.modal-icon-sec').hide();
                console.log(res);
                if (res.data && res.data.length) {
                    loadProductsTable(res.data);
                    $('.add-new-products').show();
                } else {
                    $('.add-new-products').hide();
                    loadProductsTable([]);
                }
            },
            error: function (result, xhr) {
                $('.modal-icon-sec').hide();
                $('.add-new-products').hide();
            }
        });
    }
    //build table after search results
    function loadProductsTable(mydata) {
        jQuery("#myGrid").jqGrid("clearGridData");
        jQuery("#myGrid").jqGrid({
            datatype: "local",
            height: 250,
            colNames: ['MychemId', 'Description', 'PG','PG Number'],
            colModel: [
                { name: 'MychemID', index: 'MychemID', width: 110, sorttype: "int" },
                { name: 'Description', index: 'Description', width: 360 },
                { name: 'PG', index: 'PG', width: 150 },
                { name: 'PGNumber', index: 'PGNumber', width: 110 }
            ],
            multiselect: true
        });
        for (var i = 0; i <= mydata.length; i++)
            jQuery("#myGrid").jqGrid('addRowData', i + 1, mydata[i]);
    }
    $('.duplicates-table').on('change', '.move-all', function () {
        if ($(this).prop('checked')) {
            $('.duplicates-table tbody').find('.move-single').prop('checked', true);
        } else {
            $('.duplicates-table tbody').find('.move-single').prop('checked', false);
        }
    });
    $('.duplicates-table').on('change', '.move-single', function () {
        if ($('.duplicates-table tbody').find('.move-single').length == $('.duplicates-table tbody').find('.move-single:checked').length) {
            $('.duplicates-table thead').find('.move-all').prop('checked', true);
        } else {
            $('.duplicates-table thead').find('.move-all').prop('checked', false);
        }
    });
    $('.duplicate-id-section').on('click', '.move-selected', function () {
        var Ids = $('.move-single').map(function () {
            if (!($(this).prop('checked'))) {
                return $(this).parents('tr').find('.chemId').text().trim();
            }
        }).toArray();
        $('#new-custom-pg-items>p').each(function () {
            if (Ids.indexOf($(this).find('span').text())>-1) {
                $(this).remove();
            }
        });
        duplicatePassThrough = false;
        if ($('.create-new-cg').length) {
            $('#myModal .create-new-cg').trigger('click');
        } else {
            $('#myModal .edit-cg').trigger('click');
        }
        
    });

    var duplicatePassThrough = true;
    //Save new Custom Group
    $('#myModal').on('click','.create-new-cg', function (e) {
        e.stopPropagation();
        $('.duplicate-id-section').hide();
        if ($('#new-custom-pg').val() && $('#new-custom-pg-items p').length) {
            var existingProducts = $('#new-custom-pg-items>p').map(function () {
                return $(this).find('span').text()
            }).toArray();
            var duplicateData = [];

            if (duplicatePassThrough) {
                $.ajax({
                    cache: false,
                    async: false,
                    cors: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/home/CheckCustomGroupIds",
                    dataType: "json",
                    data: JSON.stringify({
                        MychemIds: existingProducts.join(',')
                    }),
                    success: function (res) {
                        console.log(res.data);
                        duplicateData = res.data;

                    },
                    error: function (result, xhr) {
                    }
                });
            }
            



            if (duplicateData.length) {
                var duplicateStr = '';
                console.log(duplicateData);
                duplicateData.forEach(function (v) {
                    duplicateStr += '<tr><td class="chemId"> ' + v.MychemID + '</td><td>' + getCustomGroupName(v.CustomGroupId) +'</td> <td><input type="checkbox" class="move-single" name="move-cb"/></td></tr>';
                });
                $('.duplicate-id-section table tbody').html(duplicateStr)
                $('.duplicate-id-section').show();
                return false;
            } else {
                $('.loading-bar-section').show();
                $.ajax({
                    cors: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/home/CreateCustomGroup?customGroup=" + $('#new-custom-pg').val(),
                    dataType: "json",
                    success: function (res) {
                        console.log(res);
                        if (res.data == 'Group Name already exist.') {
                            $('.loading-bar-section').hide();
                            showNotification(res.data, 'danger', 'right');
                        } else {
                            $.ajax({
                                cors: true,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "/home/CustomGroupUpdateOnSourceTable?customGroup=" + res.data.Id + "&&GroupName=" + res.data.CustomGroupName,
                                dataType: "json",
                                data: JSON.stringify({
                                    Products: existingProducts.join(',')
                                }),
                                success: function (res) {
                                    duplicatePassThrough = true;
                                    $('.loading-bar-section').hide();
                                    jQuery("#myGrid").jqGrid("clearGridData");
                                    $('#new-custom-pg-items').empty();
                                    $('#new-custom-pg,#custom-pg').val('');
                                    getCustomPgList();
                                    $('.duplicate-id-section').hide();
                                    duplicatePassThrough = true;
                                    $('.save-pg').removeClass('create-new-cg').removeClass('edit-cg');
                                    showNotification("new custom group created successfully !", 'success', 'center');
                                    $("#myModal").modal('hide');                                    
                                },
                                error: function (result, xhr) {
                                    $('.loading-bar-section').hide();
                                }
                            });
                        }
                    },
                    error: function (result, xhr) {
                        $('.loading-bar-section').hide();
                    }
                });
            }            
        } else {
            showNotification("Please select the group name/select products into group !", 'danger', 'right');
        }

    });

    //Update Custom Group
    $('#myModal').on('click', '.edit-cg', function (e) {
        e.stopPropagation();
        $('.duplicate-id-section').hide();
        if ($('#new-custom-pg-items p').length) {
            var existingProducts = $('#new-custom-pg-items>p').map(function () {
                return $(this).find('span').text()
            }).toArray();
            var duplicateData = [];
            if (duplicatePassThrough) {
                $.ajax({
                    cache: false,
                    async: false,
                    cors: true,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: "/home/CheckCustomGroupIds?MychemIds=" + existingProducts.join(',') + "&&GroupId=" + $('.select-custom-pg').find('option:selected').val(),
                    dataType: "json",
                    success: function (res) {
                        console.log(res.data);
                        duplicateData = res.data;
                    },
                    error: function (result, xhr) {
                    }
                });
            }

            if (duplicateData.length) {
                var duplicateStr = '';
                console.log(duplicateData);
                duplicateData.forEach(function (v) {
                    duplicateStr += '<tr><td class="chemId"> ' + v.MychemID + '</td><td>' + getCustomGroupName(v.GroupId) + '</td> <td><input type="checkbox" class="move-single" name="move-cb"/></td></tr>';
                });
                $('.duplicate-id-section table tbody').html(duplicateStr)
                $('.duplicate-id-section').show();
                return false;
            } else {
                $('.loading-bar-section').show();
                $.ajax({
                    cors: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/home/CustomGroupUpdateOnSourceTable?customGroup=" + $('.select-custom-pg').find('option:selected').val() + "&&GroupName=" + $('.select-custom-pg').find('option:selected').text(),
                    dataType: "json",
                    data: JSON.stringify({
                        Products: existingProducts.join(',')
                    }),
                    success: function (res) {
                        $('.loading-bar-section').hide();
                        jQuery("#myGrid").jqGrid("clearGridData");
                        $('#new-custom-pg-items').empty();
                        $('#new-custom-pg,#custom-pg').val('');
                        getCustomPgList();
                        $('.duplicate-id-section').hide();
                        duplicatePassThrough = true;
                        $('.save-pg').removeClass('create-new-cg').removeClass('edit-cg');
                        showNotification('custom group updated successfully !', 'success', 'center');
                        $("#myModal").modal('hide');
                        $('.edit-custom-pg,.delete-custom-pg').hide();
                        $('.create-new-pg').show();
                        
                    },
                    error: function (result, xhr) {
                        $('.loading-bar-section').hide();
                    }
                });
            }


        } else {
            alert('Please select products into group');
        }

    });


    $('#reset-td').change(function () {
        if ($(this).prop('checked')) {
            $('#td').prop('disabled', true);
        } else {
            $('#td').prop('disabled', false);
        }
    });

    $('.close-modal').click(function () {
        jQuery("#myGrid").jqGrid("clearGridData");
        $('#new-custom-pg-items').empty();
        $('#new-custom-pg,#custom-pg').val('');
        $('.duplicate-id-section').hide();
        duplicatePassThrough = true;
        $('.save-pg').removeClass('create-new-cg').removeClass('edit-cg');
        $("#myModal").modal('hide');
    });

    $('#td').keyup(function () {
        if ($(this).val()) {
            $('#reset-td').prop('disabled', true);
        } else {
            $('#reset-td').prop('disabled', false);
        }
    });

    $('#td').on('input', function () {

        this.value = this.value.match(/^\d+\.?\d{0,2}/);

    });
    //Save Trade Agreements
    $('button[type="submit"]#save_td').click(function () {
        var selectedStore = $('.store_name_sec .sub-group input[type="radio"]:checked');
        var selectedGroup = $('.group_name_sec .sub-group input[type="radio"]:checked');
        var stores = storegroup = few_stores = exclusion = pg = pgGroups = customPg = fromDate = toDate = td = null;
        if (selectedStore.length > 0) {
            if (selectedStore.attr('selected_type') == 'store_groups') {
                if ($('#store_groups select').find('option:selected').val()) {
                    storegroup = $('#store_groups select').find('option:selected').val();
                } else {
                    alert('Please select store group');
                    return;
                }
            } else if (selectedStore.attr('selected_type') == 'few_stores') {
                var selected = $('#few_stores select option:selected').map(function (a, item) { return item.value; }).toArray();
                if (selected.length) {
                    few_stores = selected.join();
                } else {
                    alert('Please select stores');
                    return;
                }

            } else {
                stores = 'ALL';
            }
        } else {
            alert('Please select Store type');
            return;
        }


        if (selectedGroup.length > 0) {
            if (selectedGroup.attr('selected_type') == 'custom_pg') {
                if ($('#custom_pg select').find('option:selected').val()) {
                    customPg = $('#custom_pg select').find('option:selected').val();
                } else {
                    alert('Please select custom Product group');
                    return;
                }
            } else if (selectedGroup.attr('selected_type') == 'few_pg') {
                var selectedPgs = $('#few_pg select option:selected').map(function (a, item) { return item.value; }).toArray();
                if (selectedPgs.length) {
                    pgGroups = selectedPgs.join();
                } else {
                    alert('Please select Product group');
                    return;
                }

            } else {
                pg = 'ALL';
            }
        } else {
            alert('Please select PG type');
            return;
        }

        
        if ($('#reset-td').prop('checked')) {
            td = -1;
        } else {
            if ($('#td').val() && $('#td').val() > 0) {
                td = parseFloat($('#td').val()).toFixed(2);
                if (td > 100) {
                    alert('TA percentage cannot be greater than 100');
                    return;
                }
            } else {
                alert('Please select the Trade percentage');
                return;
            }
        }
        
        var isApplyTA = (td == -1 ? false : true);
        
        var tdArr = [];
        if (stores != null) {
            if (pg != null) {
                tdArr.push(tradeObjFn('ALL', null, pg, null, isApplyTA, td, 90));
            } else {
                if (pgGroups != null) {
                    var PgIds = pgGroups.split(',');
                    $.each(PgIds,function (k,v) {
                        tdArr.push(tradeObjFn(stores, null, 'PG', v, isApplyTA, td, 80));
                    });
                } else {
                    tdArr.push(tradeObjFn(stores, null, 'CG', customPg, isApplyTA, td, 70));
                }
            }
        } else {
            if (storegroup != null) {
                if (pg != null) {
                    tdArr.push(tradeObjFn('SG', storegroup, pg, null, isApplyTA, td, 60));
                } else {
                    if (pgGroups != null) {
                        var PgIds = pgGroups.split(',');
                        $.each(PgIds, function (k, v) {
                            tdArr.push(tradeObjFn('SG', storegroup, 'PG', v, isApplyTA, td, 50));
                        });

                    } else {
                        tdArr.push(tradeObjFn('SG', storegroup, 'CG', customPg, isApplyTA, td, 40));
                    }
                }
            } else {
                if (pg != null) {
                    var storeIds = few_stores.split(',');
                    $.each(storeIds, function (k, v) {
                        tdArr.push(tradeObjFn('ST', v, pg, null, isApplyTA, td, 30));
                    });


                } else {
                    if (pgGroups != null) {
                        var storeIds = few_stores.split(',');
                        var PgIds = pgGroups.split(',');
                        $.each(storeIds, function (key, val) {
                            $.each(PgIds, function (k, v) {
                                tdArr.push(tradeObjFn('ST', val, 'PG', v, isApplyTA, td, 20));
                            });
                        });


                    } else {
                        var storeIds = few_stores.split(',');
                        $.each(storeIds, function (k, v) {
                            tdArr.push(tradeObjFn('ST', v, 'CG', customPg, isApplyTA, td, 10));
                        });
                    }
                }
            }
        }

        console.log('728::', tdArr);
        if (!$('.activeEditForm').length) {
            var duplicateRules = [];
            tdArr.forEach(function (v) {
                var r = PriotiryRulesList.filter(function (p) { if (v.StoreType == p.StoreType && v.StoreTypeValue == p.StoreTypeValue && v.ProductType == p.ProductType && v.ProductTypeValue == p.ProductTypeValue) { return true; } return false; });
                if (r.length) duplicateRules = duplicateRules.concat(r);
            });
            var d = new Date();

            if (duplicateRules.length) {
                showNotification("Similar rule already exists – Please review and edit existing rule if required", 'danger');
                return;
            }
                
        }
        
        $('.loading-bar-section').show();
        $.ajax({
            cors: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/home/UpdateTradeOnBranchIfoProductsTable",
            dataType: "json",
            data: JSON.stringify(tdArr),
            success: function (res) {
                console.log(res);
                $('.loading-bar-section').hide();
                loadPriorityTable(true);
                
                showNotification("Trade Agreement successfully updated!", 'success', 'center');
                $('.ta-form').removeClass('activeEditForm');
                $('.ta-form').nextAll().css('pointer-events', 'all');
                $('#clear_td').text('Clear');
            },
            error: function (result, xhr) {
                $('.loading-bar-section').hide();
                $('.ta-form').removeClass('activeEditForm');
                $('.ta-form').nextAll().css('pointer-events', 'all');
                $('#clear_td').text('Clear');
            }
        });
    });


    function getCustomGroupName(id) {
        return CustomGroupList.find(function (v) {
            return v.Id == id;
        }).CustomGroupName.toUpperCase();
    }

    $("#search-mychemId").keydown(function (e) {
        $('.tooltiptext').css('visibility', 'hidden');
        $(this).removeClass('error-field');
        
    });

    $('button[type="submit"]#search_res').click(function () {
        var store = $('.search-branch option:selected').val();
        var MychemId = $('#search-mychemId').val();
        var url = '', isMychem = false;
        if (isNaN(MychemId)) {
            url = "/home/GetPriceDetailsOnDesc?Store=" + store + "&&desc=" + MychemId;
        } else {
            url = "/home/GetPriceDetails?Store=" + store + "&&MychemId=" + MychemId;
            isMychem = true;
        }
        if ((MychemId.length == 7 && isMychem) || (!isMychem && MychemId.length >=5)) {
            $('.loading-bar-section').show();
            $.ajax({
                cache: false,
                //async: false,
                cors: true,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: url,
                dataType: "json",
                success: function (res) {
                    console.log('res::', res);
                    if (isMychem) {
                        if (res.data == 'Invalid MychemID') {
                            $('.tooltiptext').css('visibility', 'visible');
                            $("#search-mychemId").addClass('error-field');
                            $('.search-results-table tbody').html('');
                        } else {
                            var str = '';
                            if (res.data != null && res.data != "No Data Found.") {
                                //str += '<tr><td>' + res.data.MychemId + '</td><td>' + res.data.Description + '</td><td>' + formalDataFn('decimal', res.data.Base_price) + '</td><td class=' + (res.data.Base_price == res.data.Sell_price ? '' : 'promo-price') + '>' + formalDataFn('decimal', res.data.Sell_price) + '</td><td>' + formalDataFn('decimal', res.data.Cost_price, 'cp') + '</td><td>' + formalDataFn('decimal', res.data.Rebate, 'rbt') + '</td><td>' + formalDataFn('', res.data.Trade, 'td') + '</td><td>' + formalDataFn('', res.data.gst) + '</td></tr>';
                                str += '<tr><td>' + res.data.MychemId + '</td><td>' + res.data.Description + '</td><td>' + formalDataFn('decimal', res.data.Base_price) + '</td><td>' + formalDataFn('decimal', res.data.Cost_price, 'cp') + '</td><td>' + formalDataFn('', res.data.Trade, 'td') + '</td><td>' + formalDataFn('', res.data.gst) + '</td></tr>';
                                $('.search-results-table tbody').html(str);
                            } else {
                                $('.search-results-table tbody').html('');
                                $.bootstrapGrowl(res.data, {
                                    type: 'danger'
                                });
                            }
                        }
                    } else {
                        if (res.data.length == 0) {
                            $('.tooltiptext').css('visibility', 'visible');
                            $("#search-mychemId").addClass('error-field');
                            $('.search-results-table tbody').html('');
                        } else {
                            var str = '';
                            res.data.forEach(function (v) {
                                if (v.data != null && v.data != "No Data Found.") {
                                    //str += '<tr><td>' + v.data.MychemId + '</td><td>' + v.data.Description + '</td><td>' + formalDataFn('decimal', v.data.Base_price) + '</td><td class=' + (v.data.Base_price == v.data.Sell_price ? '' : 'promo-price') + '>' + formalDataFn('decimal', v.data.Sell_price) + '</td><td>' + formalDataFn('decimal', v.data.Cost_price, 'cp') + '</td><td>' + formalDataFn('decimal', v.data.Rebate, 'rbt') + '</td><td>' + formalDataFn('', v.data.Trade, 'td') + '</td><td>' + formalDataFn('', v.data.gst) + '</td></tr>';
                                    str += '<tr><td>' + v.data.MychemId + '</td><td>' + v.data.Description + '</td><td>' + formalDataFn('decimal', v.data.Base_price) + '</td><td>' + formalDataFn('decimal', v.data.Cost_price, 'cp') + '</td><td>' + formalDataFn('', v.data.Trade, 'td') + '</td><td>' + formalDataFn('', v.data.gst) + '</td></tr>';
                                }
                            });
                            $('.search-results-table tbody').html(str);
                        }
                    }
                    
                    $('.loading-bar-section').hide();
                },
                error: function (result, xhr) {
                    $('.loading-bar-section').hide();
                }
            });

        } else {
            $('.tooltiptext').css('visibility', 'visible');
            $("#search-mychemId").addClass('error-field');
            return;
        }
    });

    function formalDataFn(type,val,param) {
        if (val == null || val == 'undefined' || (val == -1 && param == 'td')) {
            return param?'NA':'';
        }
        if (type == 'decimal') {
            return '$'+val.toFixed(2);
        }
        return val;
    }

    function loadPriorityTable(reload) {

        $.ajax({
            cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetPriotityList",
            dataType: "json",
            success: function (res) {
                if (reload) {
                    $('.priority-table').DataTable().destroy();
                    $('.priority-table tbody').empty();
                }
                PriotiryRulesList = res.data;
                var str = '';
                $.each(res.data, function (k, v) {
                    str += '<tr class=' + (v.synced == 'false' ? ' new_rules ' : '') + '"Info' + (v.Priority == 0 ? ' no-events' : '') + '"><td>' + v.Id + '</td><td><span  data-modified =' + (v.LastModifiedDateTime ? v.LastModifiedDateTime : v.CreatedDateTime) + ' class="store-selected"><i>' + getBranchName(v.StoreType, v.StoreTypeValue) + '</i></span></td><td class="pg_type" data-pg=' + v.ProductType + '>' + getPgName(v.ProductType, v.ProductTypeValue) + '</td><td class="trade-val">' + (v.DiscountPCT == null ? 'NA' : v.DiscountPCT) + '</td><td>' + (v.isApplyTA == 0 ? 'true' : 'false') + '</td><td>' + dateFn(v.LastModifiedDateTime ? v.LastModifiedDateTime : v.CreatedDateTime) + '</td><td>' + (v.synced == 'false' ? ' Ready for sync ' : 'Synced') +'</td><td class="action-icons">' + (UserRole == 'admin'?'<span class="glyphicon glyphicon-pencil edit-td' + (v.Priority == 0 ? ' no-edit' : '') + '"></span><span class="glyphicon glyphicon-trash delete-td" data-id=' + v.Id + '></span>':'')+'</td><td class="td-priority no-view">' + v.Priority + '</td></tr>';
                });
                $('.priority-table tbody').html(str);
                var priorityTable = $('.priority-table').DataTable({
                    //bFilter: false,
                    scrollY: '40vh',
                    scrollCollapse: true
                });
                priorityTable.columns.adjust();      
            },
            error: function (result, xhr) {
            }
        });
       
    }
    loadPriorityTable();


    function dateFn(dt) {
        var d = new Date(dt.match(/\d+/)[0] * 1),
            minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
            hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
            ampm = d.getHours() >= 12 ? 'pm' : 'am',
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' ' + hours + ':' + minutes + ampm;

        
    }



    function getBranchName(storeType, storeTypeValue) {

        if (storeType.toUpperCase() == 'ALL') {
            return storeType;
        } else if (storeType == 'SG') {
            return GetBrandName(storeTypeValue);
        } else if (storeType == "ST") {
            var bName = '';
            BranchList.forEach(function (v) {
                if (v.BranchID == storeTypeValue) {
                    bName = v.Storename;
                }
            });
            return bName;
        } else {
            if (storeType == 'SC') {
                if (storeTypeValue == 1) {
                    return 'AUS';
                } else {
                    return 'NZ';
                }
                
            }
            
        }
        
    }

    function getPgName(productType, productTypeValue,type) {

        if (productType.toUpperCase() == 'ALL') {
            return productType;
        } else if (productType == 'PG') {
            var pName = '';
            PgList.forEach(function (v) {
                if (v.LockiePg == productTypeValue) {
                    pName = v.Description;
                }
            });
            return pName;
        } else if (productType == 'CG') {
            var cgName = '', cgList = CustomGroupList ;
            if (type == 'post') {
                cgList = Post_CustomGroupList;
            }
            cgList.forEach(function (v) {
                if (v.Id == productTypeValue) {
                    cgName = v.CustomGroupName;
                }
            });
            return cgName;
        } else if (productType == 'Schedule') { return 'S' + productTypeValue;}
         else {
            return productType;
        }
        
        
    }
    


    //Edit the existing Trade agreement rule
    $('.priority-table tbody').on('click', '.edit-td', function () {
        $('#clear_td').text('Close');
        $('.ta-form').addClass('activeEditForm');
        $('.ta-form').nextAll().css('pointer-events', 'none');
        var md = ($(this).parents('tr').find('.store-selected').attr('data-modified')).match(/\d+/)[0] * 1;
        var store_type = $(this).parents('tr').find('.store-selected i').text().toLowerCase();
        var pg_type = $(this).parents('tr').find('.pg_type').attr('data-pg').toLowerCase();
        var pg_type_val = $(this).parents('tr').find('.pg_type').text().toLowerCase();
        var trade = $(this).parents('tr').find('.trade-val').text();
        if (trade == 'NA') {
            trade = -1;
        }
        var priority = $(this).parents('tr').find('.td-priority').text();

        $(".ams_stores select option,.ams_pg select option").prop("selected", false);
        $("#few_pg select,#few_stores select,#all_stores select").multiselect("refresh");

        $('#td').val('').prop('disabled', false);
        $("#reset-td").prop("checked", false).prop('disabled', false);

        $('.create-new-pg').show();
        $('.edit-custom-pg,.delete-custom-pg').hide();


        if (store_type == 'all') {
            $('.store_name_sec input[type="radio"][selected_type="all_stores"]').trigger('click');
        } else if (store_type == 'chemist warehouse' || store_type == 'my chemist' || store_type == 'my beauty spot') {
            $('.store_name_sec input[type="radio"][selected_type="store_groups"]').trigger('click');
            var brandId = 0;
            if (store_type == 'chemist warehouse') {
                brandId = 2;
            } else if (store_type == 'my chemist') {
                brandId = 1;
            } else {
                brandId = 3;
            }
            $("#store_groups select option[value='" + brandId + "']")
                .prop("selected", true);
        } else {
            $('.store_name_sec input[type="radio"][selected_type="few_stores"]').trigger('click');
            var brId = BranchList.find(function (v) {
                return v.Storename.toLowerCase() == store_type;
            }).BranchID;
            $("#few_stores select option[value='" + brId + "']")
                .prop("selected", true);
            $("#few_stores select").multiselect("refresh");
        }



        if (pg_type == 'all') {
            $('.group_name_sec input[type="radio"][selected_type="all_pg"]').trigger('click');
        } else if (pg_type == 'cg') {
            $('.group_name_sec input[type="radio"][selected_type="custom_pg"]').trigger('click');

            var cg = CustomGroupList.find(function (v) {
                return v.CustomGroupName.toLowerCase() == pg_type_val;
            }).Id;
            $("#custom_pg select option[value='" + cg + "']").prop("selected", true);
            $('.create-new-pg').hide();
            $('.edit-custom-pg,.delete-custom-pg').show();
        } else {
            $('.group_name_sec input[type="radio"][selected_type="few_pg"]').trigger('click');
            var pgId = 0;
            PgList.forEach(function (v) {
                if (v.Description.toLowerCase() == pg_type_val) {
                    pgId = v.LockiePg;
                }
            });
            $("#few_pg select option[value='" + pgId + "']")
                .prop("selected", true);
            $("#few_pg select").multiselect("refresh");
        }
        if (trade == -1) {
            $('#td').val('').prop('disabled', true);
            $("#reset-td").prop("checked", true).prop('disabled', false);
        } else {
            $('#td').val(trade);
            $("#reset-td").prop("checked", false).prop('disabled', true);
        }
        


        $('.priority-table tbody tr').removeClass('active-row');
        $(this).parents('tr').addClass('active-row');
        

        //$(window).scrollTop(0);
        $("html, body").animate({ scrollTop: "0px" });
    });
    //Edit the existing Trade agreement rule
    $('.priority-table tbody').on('click', '.delete-td', function () {
        console.log($(this).attr('data-id'));
        if (confirm("Are you sure you want to delete a rule?")) {
            $.ajax({
                cors: true,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/home/DeleteTradeRule?Id=" + $(this).attr('data-id'),
                dataType: "json",
                success: function (res) {
                    showNotification(res.data, 'success','center')
                    loadPriorityTable(true);
                },
                error: function (result, xhr) {
                    $('.loading-bar-section').hide();
                }
            });
        } else {
            
        }
    });
    

    function GetBrandName(digit) {
        switch (digit) {
            case 1: return "My Chemist";
            case 2: return "Chemist Warehouse";
            case 3: return "My Beauty Spot";
        }
        return "";
    }

    function showNotification(msg, type, align) {
      return $.bootstrapGrowl(msg, {
                ele: 'body',
                type: type,
                offset: { from: 'top', amount: 10 },
                align: align ? align: 'center',
                width: type == 'success'? 1000: 600,
                delay: 6000,
                allow_dismiss: true,
                stackup_spacing: 10
            });
    }

    $('#sync_changes').on('click', function () {
        loadDeletedPriorityTable();
        getPostCustomPgList();
        var sync_data = PriotiryRulesList.filter(x => x.synced == 'false');
        var sync_data_del = PriotiryDeletedRulesList.filter(x => x.postRule == 'Delete');

        if (sync_data.length == 0 && sync_data_del.length == 0) {
            showNotification('No changes to Sync.', 'danger', 'center');
            return;
        }


        var str = '';
        $.each(sync_data, function (k, v) {
            str += '<tr class="Info"><td>' + v.Id + '</td><td>' + getBranchName(v.StoreType, v.StoreTypeValue) + '</td><td class="pg_type" data-pg=' + v.ProductType + '>' + getPgName(v.ProductType, v.ProductTypeValue) + '</td><td class="trade-val">' + (v.DiscountPCT == null ? 'NA' : v.DiscountPCT) + '</td><td>' + (v.isApplyTA == 0 ? 'true' : 'false') + '</td><td>' + (v.postRule == 'New' ? 'Insert' : 'Update') + '</td></tr>';
        });

        $.each(sync_data_del, function (k, v) {
            str += '<tr class="Info"><td>' + v.Id + '</td><td>' + getBranchName(v.StoreType, v.StoreTypeValue) + '</td><td class="pg_type" data-pg=' + v.ProductType + '>' + getPgName(v.ProductType, v.ProductTypeValue,'post') + '</td><td class="trade-val">' + (v.DiscountPCT == null ? 'NA' : v.DiscountPCT) + '</td><td>' + (v.isApplyTA == 0 ? 'true' : 'false') + '</td><td>' + (v.postRule) + '</td></tr>';
        });


        $('#myModal_sync').modal({
            backdrop: 'static',
            keyboard: false
        });

        $('#confirm_rules_table').DataTable().destroy();
        $('#confirm_rules_table tbody').empty();

        $('#confirm_rules_table tbody').html(str);
        console.log(sync_data);

        var syncTable = $('#confirm_rules_table').DataTable({
            //bFilter: false,
            scrollY: '40vh',
            scrollCollapse: true
        });

        setTimeout(function () {
           
            syncTable.columns.adjust();  
        },200);

          
            
        
    });




    $('#sync_with_prod').on('click',function () {

        $('.loading-bar-section').show();
        $.ajax({
            cors: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/home/SyncChanges",
            dataType: "json",
            success: function (res) {
                $('.loading-bar-section').hide();
                if (res.data == 0) {
                    showNotification('Rules successfully synced.', 'success', 'center');
                    loadPriorityTable(true);
                } else {
                    showNotification('Failed to sync the rules.', 'danger', 'center')
                }
                $('#myModal_sync').modal("hide");
            },
            error: function (result, xhr) {
                $('.loading-bar-section').hide();
            }
        });
    });


});



function tradeObjFn(StoreType, StoreTypeValue, ProductType, ProductTypeValue, isApplyTA, DiscountPCT, Priority) {

    var tdObj = {
        StoreType: StoreType,
        StoreTypeValue: StoreTypeValue,
        ProductType: ProductType,
        ProductTypeValue: ProductTypeValue,
        isApplyTA : isApplyTA,
        DiscountPCT: DiscountPCT == -1 ? null : DiscountPCT,
        Priority: Priority,
        CreatedDateTime: null,
        CreatedBy: null
    };

    
    return tdObj;
}









    








PriotiryDeletedRulesList = [];

function loadDeletedPriorityTable(reload) {

    $.ajax({
        cache: false,
        async: false,
        cors: true,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/home/GetDeletedPriotityList",
        dataType: "json",
        success: function (res) {
            PriotiryDeletedRulesList = res.data;
        },
        error: function (result, xhr) {
        }
    });

}

Post_CustomGroupList = []
function getPostCustomPgList() {
    $.ajax({
        //cache: false,
        async: false,
        cors: true,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/home/GetPostCustomPgList",
        dataType: "json",
        success: function (res) {
             
            Post_CustomGroupList = res.data;
        },
        error: function (result, xhr) {
        }
    });
}